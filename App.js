import React, { Component } from 'react';
import { createStackNavigator } from 'react-navigation';

import HomeScreen from './screens/HomeScreen';
import DetailsScreen from './screens/DetailsScreen';
import ActivityIndicatorScreen from './screens/ActivityIndicatorScreen';
import ButtonScreen from './screens/ButtonScreen';
import DrawerLayoutAndroidScreen from './screens/DrawerLayoutAndroidScreen';
import ImageScreen from './screens/ImageScreen';
import KeyboardAvoidingViewScreen from './screens/KeyboardAvoidingViewScreen';
import ListViewScreen from './screens/ListViewScreen';
import ModalScreen from './screens/ModalScreen';
import ProgressBarAndroidScreen from './screens/ProgressBarAndroidScreen';
import RefreshControlScreen from './screens/RefreshControlScreen';
import SectionListScreen from './screens/SectionListScreen';
import SliderScreen from './screens/SliderScreen';
import StatusBarScreen from './screens/StatusBarScreen';
import TextScreen from './screens/TextScreen';
import TextInputScreen from './screens/TextInputScreen';
import TouchableHighlightScreen from './screens/TouchableHighlightScreen';
import TouchableOpacityScreen from './screens/TouchableOpacityScreen';
import ViewScreen from './screens/ViewScreen';
import WebViewScreen from './screens/WebViewScreen';

const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Details: {
      screen: DetailsScreen,
    },
    ActivityIndicator: {
      screen: ActivityIndicatorScreen,
    },
    Button: {
      screen: ButtonScreen,
    },
    DLA: {
      screen: DrawerLayoutAndroidScreen,
    },
    Image: {
      screen: ImageScreen,
    },
    KAV: {
      screen: KeyboardAvoidingViewScreen,
    },
    ListView: {
      screen: ListViewScreen,
    },
    Modal: {
      screen: ModalScreen,
    },
    PBA: {
      screen: ProgressBarAndroidScreen,
    },
    RefreshControl: {
      screen: RefreshControlScreen,
    },
    
    SectionList: {
      screen: SectionListScreen,
    },
    Slider: {
      screen: SliderScreen,
    },
    StatusBar: {
      screen: StatusBarScreen,
    },
    Text: {
      screen: TextScreen,
    },
    TextInput: {
      screen: TextInputScreen,
    },
    TH: {
      screen: TouchableHighlightScreen,
    },
    
    TO: {
      screen: TouchableOpacityScreen,
    },
    
    View: {
      screen: ViewScreen,
    },
    WebView: {
      screen: WebViewScreen,
    },
  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends Component {
  render() {
    return <RootStack />
  }
}
