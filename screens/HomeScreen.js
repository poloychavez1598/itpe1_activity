import React, { Component } from 'react';
import { StyleSheet, View, Button, ScrollView } from 'react-native';

export default class HomeScreen extends Component {
  render() {
    return (
      <ScrollView>
        <View style={[styles.container, styles.spacing]}>
          <Button title="Details" onPress={() => this.props.navigation.navigate('Details')} />
          <Button title="ActivityIndicator" onPress={() => this.props.navigation.navigate('ActivityIndicator')} />
          <Button title="Button" onPress={() => this.props.navigation.navigate('Button')} />
          <Button title="DrawerLayoutAndroid" onPress={() => this.props.navigation.navigate('DLA')} />
          <Button title="Image" onPress={() => this.props.navigation.navigate('Image')} />
          <Button title="KeyboardAvoidingView" onPress={() => this.props.navigation.navigate('KAV')} />
          <Button title="ListView" onPress={() => this.props.navigation.navigate('ListView')} />
          <Button title="Modal" onPress={() => this.props.navigation.navigate('Modal')} />
          <Button title="Picker" onPress={() => this.props.navigation.navigate('Picker')} />
          <Button title="ProgressBarAndroid" onPress={() => this.props.navigation.navigate('PBA')} />
          <Button title="RefreshControl" onPress={() => this.props.navigation.navigate('RefreshControl')} />
          <Button title="ScrollView" onPress={() => this.props.navigation.navigate('ScrollView')} />
          <Button title="SectionList" onPress={() => this.props.navigation.navigate('SectionList')} />
          <Button title="Slider" onPress={() => this.props.navigation.navigate('Slider')} />
          <Button title="StatusBar" onPress={() => this.props.navigation.navigate('StatusBar')} />
          <Button title="Switch" onPress={() => this.props.navigation.navigate('Switch')} />
          <Button title="Text" onPress={() => this.props.navigation.navigate('Text')} />
          <Button title="TextInput" onPress={() => this.props.navigation.navigate('TextInput')} />
          <Button title="TouchableHighlight" onPress={() => this.props.navigation.navigate('TH')} />
          <Button title="TouchableNativeFeedback" onPress={() => this.props.navigation.navigate('TNF')} />
          <Button title="TouchableOpacity" onPress={() => this.props.navigation.navigate('TO')} />
          <Button title="TouchableWithoutFeedback" onPress={() => this.props.navigation.navigate('TWF')} />
          <Button title="View" onPress={() => this.props.navigation.navigate('View')} />
          <Button title="ViewPagerAndroid" onPress={() => this.props.navigation.navigate('VPA')} />
          <Button title=WebView" onPress={() => this.props.navigation.navigate('WebView')} />
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  spacing: {
    padding: 20
  },
});
